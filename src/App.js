import React from "react";
import axios from "axios";
import MuscleSelection from "./MuscleSelection.js";
import ExerciseSelection from "./ExerciseSelection.js";
import RoutineSelection from "./RoutineSelection.js";

function App() {
  const [muscles, setMuscles] = React.useState([]);
  const [exercises, setExercises] = React.useState([]);
  const [split, setSplit] = React.useState(1);
  const [daysOfTraining, setDaysOfTraining] = React.useState(1);

  React.useEffect(function () {
    axios.get(process.env.REACT_APP_EXERCISE_API + "muscle/").then(function (response) {
      const fetchedMuscles = response.data.results.map((muscle) => ({
        ...muscle,
        isSelected: false,
      }));
      setMuscles(fetchedMuscles);
    });
  }, []);

  React.useEffect(function () {
    axios
      .get(process.env.REACT_APP_EXERCISE_API + "exercise/?language=2")
      .then(function (response) {
        const fetchedExercises = response.data.results.map((exercise) => ({
          ...exercise,
          isSelected: false,
        }));
        setExercises(fetchedExercises);
      });
  }, []);

  function chooseMuscle(event) {
    const updatedMuscles = muscles.map((muscle) => {
      if (muscle.name === event.target.name) {
        return {
          ...muscle,
          isSelected: !muscle.isSelected,
        };
      } else return muscle;
    });
    setMuscles(updatedMuscles);
  }

  function chooseExercise(event) {
    const updatedExercises = exercises.map((exercise) => {
      if (exercise.name === event.target.name) {
        return {
          ...exercise,
          isSelected: !exercise.isSelected,
        };
      } else return exercise;
    });
    setExercises(updatedExercises);
  }

  function onSplitChange(event) {
    setSplit(event.target.value);
  }

  function onDaysOfTrainingChange(event) {
    setDaysOfTraining(event.target.value);
  }

  return (
    <div className="App">
      <MuscleSelection muscles={muscles} chooseMuscle={chooseMuscle}></MuscleSelection>
      <ExerciseSelection
        exercises={exercises}
        chooseExercise={chooseExercise}
        muscles={muscles}
      ></ExerciseSelection>
      <RoutineSelection
        split={split}
        onSplitChange={onSplitChange}
        onDaysOfTrainingChange={onDaysOfTrainingChange}
        daysOfTraining={daysOfTraining}
      ></RoutineSelection>
    </div>
  );
}

export default App;
