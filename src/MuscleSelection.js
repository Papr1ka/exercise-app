import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

function MuscleSelection(props) {
  return (
    <div>
      <h1>Valitse lihas</h1>
      <form>
        {props.muscles.map((muscle) => (
          <div key={muscle.id}>
            <FormControlLabel
              control={
                <Checkbox
                  checked={muscle.isSelected}
                  onChange={props.chooseMuscle}
                  name={muscle.name}
                  color="primary"
                />
              }
              label={muscle.name}
            />
          </div>
        ))}
      </form>
    </div>
  );
}

export default MuscleSelection;
