import React from "react";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";

function ExerciseSelection(props) {
  const selectedMuscleIds = props.muscles
    .filter((muscle) => muscle.isSelected)
    .map((muscle) => muscle.id);
  const filteredExercises = props.exercises.filter((exercise) => {
    return exercise.muscles.some((muscles) => selectedMuscleIds.includes(muscles));
  });
  return (
    <div>
      <h2>Harjoitteet</h2>
      {filteredExercises.map((exercise) => (
        <div key={exercise.id}>
          <FormControlLabel
            control={
              <Checkbox
                checked={exercise.isSelected}
                onChange={props.chooseExercise}
                name={exercise.name}
                color="primary"
              />
            }
            label={exercise.name}
          />
        </div>
      ))}
    </div>
  );
}

export default ExerciseSelection;
