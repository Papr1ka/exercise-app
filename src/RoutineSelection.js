import React from "react";
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormHelperText from "@material-ui/core/FormHelperText";
import FormControl from "@material-ui/core/FormControl";
import Select from "@material-ui/core/Select";

function RoutineSelection(props) {
  return (
    <div>
      <h2>Treeniohjelma</h2>
      <FormControl>
        <InputLabel id="demo-simple-select-label">Treenijako</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={props.split}
          onChange={props.onSplitChange}
        >
          <MenuItem value={1}>1-jakoinen</MenuItem>
          <MenuItem value={2}>2-jakoinen</MenuItem>
          <MenuItem value={3}>3-jakoinen</MenuItem>
        </Select>
      </FormControl>
      <FormControl>
        <InputLabel id="demo-simple-select-label">Treenejä viikossa</InputLabel>
        <Select
          labelId="demo-simple-select-label"
          id="demo-simple-select"
          value={props.daysOfTraining}
          onChange={props.onDaysOfTrainingChange}
        >
          <MenuItem value={1}>Yksi</MenuItem>
          <MenuItem value={2}>Kaksi</MenuItem>
          <MenuItem value={3}>Kolme</MenuItem>
          <MenuItem value={4}>Neljä</MenuItem>
          <MenuItem value={5}>Viisi</MenuItem>
        </Select>
      </FormControl>
    </div>
  );
}

export default RoutineSelection;
